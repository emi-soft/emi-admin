<?php

use yii\db\Migration;

/**
 * Class m190506_132746_init
 */
class m190506_132746_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%module}}', [
            'include' => 'all',
            'type' => 'admin',
            'url' => 'admin',
            'path' => 'admin',
            'status' => true,
        ]);

        $this->insert('{{%module_config}}', [
            'module_id' => 1,
            'value' => '{"class":"emi\\\\admin\\\\Module"}'
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%module}}', ['id' => 1]);

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190506_132746_init cannot be reverted.\n";

        return false;
    }
    */
}
