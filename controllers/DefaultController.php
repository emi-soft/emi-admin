<?php

namespace emi\admin\controllers;

use engine\base\controllers\BaseModuleController;
use Yii;

/**
 * Default controller for the `core` module
 */
class DefaultController extends BaseModuleController
{
    public $layout = '@templates/basic/layouts/admin';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}
